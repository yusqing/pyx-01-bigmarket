package com.itxixi.test.infrastructure;

import com.itxixi.infrastructure.persistent.dao.IAwardDao;
import com.itxixi.infrastructure.persistent.po.Award;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Pyx 2024/10/18
 * @description 奖品持久化单元测试
 * @create 2024-10-18 下午2:37
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class AwardDaoTest {

    @Resource
    private IAwardDao iAwardDao;

    @Test
    public void test_queryAwardList() {
        List<Award> awardList = iAwardDao.queryAwardList();
        for (Award award : awardList) {
            log.info(award.toString());
        }
    }
}
