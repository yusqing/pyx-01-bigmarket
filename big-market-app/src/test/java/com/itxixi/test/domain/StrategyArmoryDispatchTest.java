package com.itxixi.test.domain;

import com.esotericsoftware.minlog.Log;
import com.itxixi.domain.strategy.service.armory.IStrategyArmory;
import com.itxixi.domain.strategy.service.armory.IStrategyDispatch;
import com.itxixi.infrastructure.persistent.redis.IRedisService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RMap;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author Pyx 2024/10/23
 * @description
 * @create 2024-10-23 下午11:49
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class StrategyArmoryDispatchTest {
    @Resource
    IStrategyArmory iStrategyArmory;
    @Resource
    IStrategyDispatch iStrategyDispatch;

    @Before//别的测试之前先运行
    public void test_strategyArmory() {
        boolean success = iStrategyArmory.assembleLotteryStrategy(100001L);
        log.info("测试结果：{}",success);
        //0.5 0.1 0.01  total:0.61 rateRange:0.61 / 0.01 = 61
        //0.5*61 = 30.5 -> 31  0.1->7  0.01->1   table.size() == 39 动态概率 size有可能超过rateRange
    }

    @Test
    public void test_getAssembleRandomVal() {
        log.info("测试结果：{}－奖品ID值", iStrategyDispatch.getRandomAwardId(100001L));
        log.info("测试结果：{}-奖品ID值", iStrategyDispatch.getRandomAwardId(100001L));
        log.info("测试结果：{}-奖品ID值", iStrategyDispatch.getRandomAwardId(100001L));
    }

    @Test
    public void test_getRandomAwardId_ruleWeightValue() {
        log.info("测试结果：{} - 4000 策略配置", iStrategyDispatch.getRandomAwardId(100001L, "4000:102,103,104,105"));
        log.info("测试结果：{} - 5000 策略配置", iStrategyDispatch.getRandomAwardId(100001L, "5000:102,103,104,105,106,107"));
        log.info("测试结果：{} - 6000 策略配置", iStrategyDispatch.getRandomAwardId(100001L, "6000:102,103,104,105,106,107,108,109"));
    }
    @Resource
    private IRedisService redisService;

    @Test
    public void test_map() {
        RMap<Integer, Integer> map = redisService.getMap("strategy_id_100001");
        map.put(1, 101);
        map.put(2, 101);
        map.put(3, 101);
        map.put(4, 102);
        map.put(5, 102);
        map.put(6, 102);
        map.put(7, 103);
        map.put(8, 103);
        map.put(9, 104);
        map.put(10, 105);

        log.info("测试结果：{}", redisService.getMap("strategy_id_100001").get(1));
    }
}
