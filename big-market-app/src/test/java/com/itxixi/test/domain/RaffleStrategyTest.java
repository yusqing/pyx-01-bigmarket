package com.itxixi.test.domain;


import com.alibaba.fastjson.JSON;
import com.itxixi.domain.strategy.model.entity.RaffleAwardEntity;
import com.itxixi.domain.strategy.model.entity.RaffleFactorEntity;
import com.itxixi.domain.strategy.service.IRaffleStrategy;
import com.itxixi.domain.strategy.service.armory.IStrategyArmory;
import com.itxixi.domain.strategy.service.armory.IStrategyDispatch;
import com.itxixi.domain.strategy.service.rule.impl.RuleWeightLogicFilter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import javax.annotation.Resource;

/**
 * @author Fuzhengwei bugstack.cn @小傅哥
 * @description 抽奖策略测试
 * @create 2024-01-06 13:28
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class RaffleStrategyTest {

    @Resource
    private IRaffleStrategy raffleStrategy;

    @Resource
    private RuleWeightLogicFilter ruleWeightLogicFilter;

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(ruleWeightLogicFilter, "userScore", 40500L);
    }

    @Resource
    IStrategyArmory iStrategyArmory;

    @Resource
    IStrategyDispatch iStrategyDispatch;

    @Before//别的测试之前先运行
    public void test_strategyArmory() {
        boolean success = iStrategyArmory.assembleLotteryStrategy(100001L);
        log.info("测试结果：{}",success);
        //0.5 0.1 0.01  total:0.61 rateRange:0.61 / 0.01 = 61
        //0.5*61 = 30.5 -> 31  0.1->7  0.01->1   table.size() == 39 动态概率 size有可能超过rateRange
    }

    @Test
    public void test_performRaffle() {
        RaffleFactorEntity raffleFactorEntity = RaffleFactorEntity.builder()
                .userId("xiaofuge")
                .strategyId(100001L)
                .build();

        RaffleAwardEntity raffleAwardEntity = raffleStrategy.performRaffle(raffleFactorEntity);

        log.info("请求参数：{}", JSON.toJSONString(raffleFactorEntity));
        log.info("测试结果：{}", JSON.toJSONString(raffleAwardEntity));
    }

    @Test
    public void test_performRaffle_blacklist() {
        RaffleFactorEntity raffleFactorEntity = RaffleFactorEntity.builder()
                .userId("user003")  // 黑名单用户 user001,user002,user003
                .strategyId(100001L)
                .build();

        RaffleAwardEntity raffleAwardEntity = raffleStrategy.performRaffle(raffleFactorEntity);

        log.info("请求参数：{}", JSON.toJSONString(raffleFactorEntity));
        log.info("测试结果：{}", JSON.toJSONString(raffleAwardEntity));
    }

}
