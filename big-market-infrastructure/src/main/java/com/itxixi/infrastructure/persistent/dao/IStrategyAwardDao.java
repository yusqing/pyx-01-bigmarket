package com.itxixi.infrastructure.persistent.dao;

import com.itxixi.infrastructure.persistent.po.Award;
import com.itxixi.infrastructure.persistent.po.StrategyAward;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Pyx 2024/10/18
 * @description 抽奖策略奖品明细配置 DAO
 * @create 2024-10-18 下午12:07
 */

@Mapper
public interface IStrategyAwardDao {

    List<Award> queryStrategyAwardList();

    List<StrategyAward> queryStrategyAwardListByStrategyId(Long strategyId);
}
