package com.itxixi.infrastructure.persistent.dao;

import com.itxixi.infrastructure.persistent.po.Award;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Pyx 2024/10/18
 * @description 奖品表 DAO
 * @create 2024-10-18 下午12:04
 */

@Mapper
public interface IAwardDao {

    List<Award> queryAwardList();
}
