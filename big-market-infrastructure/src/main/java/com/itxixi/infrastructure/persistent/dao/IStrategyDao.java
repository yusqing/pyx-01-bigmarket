package com.itxixi.infrastructure.persistent.dao;

import com.itxixi.infrastructure.persistent.po.Award;
import com.itxixi.infrastructure.persistent.po.Strategy;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Pyx 2024/10/18
 * @description 抽奖策略 DAO
 * @create 2024-10-18 下午12:06
 */

@Mapper
public interface IStrategyDao {

    List<Award> queryStrategyList();

    Strategy queryStrategyByStrategyId(Long strategyId);
}
