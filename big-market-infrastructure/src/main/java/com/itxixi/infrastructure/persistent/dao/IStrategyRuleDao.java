package com.itxixi.infrastructure.persistent.dao;

import com.itxixi.infrastructure.persistent.po.Award;
import com.itxixi.infrastructure.persistent.po.StrategyRule;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Pyx 2024/10/18
 * @description 策略规则 DAO
 * @create 2024-10-18 下午2:21
 */

@Mapper
public interface IStrategyRuleDao {

    List<Award> queryStrategyRuleList();


    StrategyRule queryStrategyRule(StrategyRule strategyRuleReq);

    String queryStrategyRuleValue(StrategyRule strategyRule);
}
