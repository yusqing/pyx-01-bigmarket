package com.itxixi.domain.strategy.model.entity;

import lombok.Data;

/**
 * @author Pyx 2024/10/30
 * @description 规则物料实体对象，用于过滤规则的必要参数信息
 * @create 2024-10-30 上午1:14
 */
@Data
public class RuleMatterEntity { //对应StrategyRuleEntity 多了个userId

    /**用户ID*/
    private String userId;

    /**策略ID*/
    private Long strategyId;

    /**抽奖奖品ID【规则类型为策略，则不需要奖品ID*/
    private Integer awardId;

    /**抽奖规则类型【ruLe_random-随机值计算、ruLe_Lock－抽奖几次后解锁、ruLe_Luck_award－幸运奖（兜底奖品)】*/
    private String ruleModel;

}
