package com.itxixi.domain.strategy.repository;

import com.itxixi.domain.strategy.model.entity.StrategyAwardEntity;
import com.itxixi.domain.strategy.model.entity.StrategyEntity;
import com.itxixi.domain.strategy.model.entity.StrategyRuleEntity;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * @author Pyx 2024/10/23
 * @description 策略仓储接口
 * @create 2024-10-23 下午6:03
 */

public interface IStrategyRepository {

    //对应数据持久层strategyAwardDao.queryStrategyAwardListByStrategyId(strategyId)
    List<StrategyAwardEntity> queryStrategyAwardList(Long strategyId);

    void storeStrategyAwardSearchRateTables(String key, Integer rateRange , HashMap<Integer, Integer> shuffleStrategyAwardSearchRateTables);

//    void storeStrategyAwardSearchRateTables(Long strategyId, BigDecimal rateRange, HashMap<Integer, Integer> shuffleStrategyAwardSearchRateTables);

    int getRateRange(Long strategyId);

    int getRateRange(String key);

    Integer getStrategyAwardAssemble(String key, int rateKey);
//---------------------------------------------------------------------------------------------------------
    StrategyEntity queryStrategyEntityByStrategyId(Long strategyId);

    StrategyRuleEntity queryStrategyRule(Long strategyId, String ruleModel);
//-----------------------------------------------------------------------------------------------------------
    String queryStrategyRuleValue(Long strategyId, Integer awardId, String ruleModel);

}
