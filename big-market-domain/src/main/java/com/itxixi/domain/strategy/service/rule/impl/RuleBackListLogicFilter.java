package com.itxixi.domain.strategy.service.rule.impl;


import com.itxixi.domain.strategy.model.entity.RuleActionEntity;
import com.itxixi.domain.strategy.model.entity.RuleMatterEntity;
import com.itxixi.domain.strategy.model.valobj.RuleLogicCheckTypeVO;
import com.itxixi.domain.strategy.repository.IStrategyRepository;
import com.itxixi.domain.strategy.service.annotation.LogicStrategy;
import com.itxixi.domain.strategy.service.rule.ILogicFilter;
import com.itxixi.domain.strategy.service.rule.factory.DefaultLogicFactory;
import com.itxixi.types.common.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author Fuzhengwei bugstack.cn @小傅哥
 * @description 【抽奖前规则】黑名单用户过滤规则
 * @create 2024-01-06 13:19
 */
@Slf4j
@Component      //自定义注解 注入之后转map对象进行使用的
                // 通过此注解，DefaultLogicFactory可以识别该类为黑名单过滤器，并将其注册到对应的logicFilterMap 中。
@LogicStrategy(logicMode = DefaultLogicFactory.LogicModel.RULE_BLACKLIST)
                //具体策略                                    //对应抽奖前规则
public class RuleBackListLogicFilter implements ILogicFilter<RuleActionEntity.RaffleBeforeEntity> {

    @Resource
    private IStrategyRepository repository;

    @Override//RuleActionEntity ：code info ruleModel data(strategyId awardId ruleWeightValueKey）     RuleMatterEntity : userId strategyId awardId ruleModel
    public RuleActionEntity<RuleActionEntity.RaffleBeforeEntity> filter(RuleMatterEntity ruleMatterEntity) {

        log.info("规则过滤-黑名单 userId:{} strategyId:{} ruleModel:{}", ruleMatterEntity.getUserId(), ruleMatterEntity.getStrategyId(), ruleMatterEntity.getRuleModel());
        String userId = ruleMatterEntity.getUserId();
        // 查询规则值配置   1.查到100：user001，user002，user003

                                    // 根据仓储 Long strategyId, Integer awardId, String ruleModel
                                    //查询IStrategyRuleDao 根据strategyRuleEntity实体 得到rule_value 100：user001，user002，user003
        String ruleValue = repository.queryStrategyRuleValue(ruleMatterEntity.getStrategyId(),
                ruleMatterEntity.getAwardId(), ruleMatterEntity.getRuleModel());
        String[] splitRuleValue = ruleValue.split(Constants.COLON);//100、user001，user002，user003
        Integer awardId = Integer.parseInt(splitRuleValue[0]);//100

        // 过滤其他规则    2. 判定useId是否属于：user001，user002，user003
        String[] userBlackIds = splitRuleValue[1].split(Constants.SPLIT);//user001，user002，user003
        for (String userBlackId : userBlackIds) {
            if (userId.equals(userBlackId)) {//如果等于 说明当前用户id是黑名单 则直接接管
                return RuleActionEntity.<RuleActionEntity.RaffleBeforeEntity>builder()
                        .ruleModel(DefaultLogicFactory.LogicModel.RULE_BLACKLIST.getCode())//"rule_blacklist"
                        .data(RuleActionEntity.RaffleBeforeEntity.builder()
                                .strategyId(ruleMatterEntity.getStrategyId())//100001
                                .awardId(awardId)//100 意思黑名单直接去Award表里 返回awardId对应的返回awardConfig 1积分
                                .build()) //data要放入一个T RuleActionEntity子类对象
                        .code(RuleLogicCheckTypeVO.TAKE_OVER.getCode())//修改状态 接管后续的流程
                        .info(RuleLogicCheckTypeVO.TAKE_OVER.getInfo())//修改状态 接管后续的流程
                        .build();
            }
        }
        //黑名单userBlackIds为空 放行 ruleModel空  T data空
        return RuleActionEntity.<RuleActionEntity.RaffleBeforeEntity>builder()
                .code(RuleLogicCheckTypeVO.ALLOW.getCode())
                .info(RuleLogicCheckTypeVO.ALLOW.getInfo())
                .build();
    }

}
