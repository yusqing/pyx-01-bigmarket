package com.itxixi.domain.strategy.service.rule;

import com.itxixi.domain.strategy.model.entity.RuleActionEntity;
import com.itxixi.domain.strategy.model.entity.RuleMatterEntity;

/**
 * @author Fuzhengwei bugstack.cn @小傅哥
 * @description 抽奖规则过滤接口
 * @create 2024-01-06 09:55
 */                                 //泛型必须继承我指定的类型
public interface ILogicFilter<T extends RuleActionEntity.RaffleEntity> {
   //过滤完规则之后返回一个动作实体   //物料是过滤规则的必备参数
    RuleActionEntity<T> filter(RuleMatterEntity ruleMatterEntity);

}
