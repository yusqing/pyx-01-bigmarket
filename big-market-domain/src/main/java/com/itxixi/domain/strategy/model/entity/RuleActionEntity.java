package com.itxixi.domain.strategy.model.entity;


import com.itxixi.domain.strategy.model.valobj.RuleLogicCheckTypeVO;
import lombok.*;

/**
 * @author Fuzhengwei bugstack.cn @小傅哥
 * @description 规则动作实体 过滤完规则之后返回一个动作实体  会影响下面流程的变化
 *              比如过滤完规则 黑名单 or 指定抽奖哪个范围
 * @create 2024-01-06 09:47
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor                  //指定传过来的泛型类型 必须是继承RaffleEntity的
                                    //意思我提供几个类你就能传几个类
public class RuleActionEntity<T extends RuleActionEntity.RaffleEntity> {

    private String code = RuleLogicCheckTypeVO.ALLOW.getCode();//通过或者拦截
    private String info = RuleLogicCheckTypeVO.ALLOW.getInfo();//默认放行 如果被接管了 就会被改变
    private String ruleModel;//黑名单 or 权重
    private T data;//表示build创建RuleActionEntity对象时必须传入T  T必须是RaffleEntity 的子类

    static public class RaffleEntity {//相当于父类  因为是静态类 所以可以用.调用RuleActionEntity.RaffleEntity

    }

    // 抽奖之前  因为继承了
    //表示在生成equals（）和hashCode（）方法时，也考虑父类的字段。这对比较两个对象是否相等时很有用。
    @EqualsAndHashCode(callSuper = true)
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    static public class RaffleBeforeEntity extends RaffleEntity {
        /**
         * 策略ID 100001
         * //   抽奖之前
         *       只用于getRandomAwardId(Long strategyId, String ruleWeightValue);;
         */
        private Long strategyId;


        /**
         * 权重值Key；用于抽奖时可以选择权重抽奖。 ruleweight 4000 5000
         * //用于getRandomAwardId(Long strategyId, String ruleWeightValue);
         */
        private String ruleWeightValueKey;


        /**
         * 奖品ID；
         */
        private Integer awardId;//黑名单直接返回奖品 不用抽
    }

    // 抽奖之中
    static public class RaffleCenterEntity extends RaffleEntity {

    }

    // 抽奖之后
    static public class RaffleAfterEntity extends RaffleEntity {

    }

}
