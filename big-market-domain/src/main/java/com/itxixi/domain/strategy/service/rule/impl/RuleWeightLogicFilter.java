package com.itxixi.domain.strategy.service.rule.impl;


import com.itxixi.domain.strategy.model.entity.RuleActionEntity;
import com.itxixi.domain.strategy.model.entity.RuleMatterEntity;
import com.itxixi.domain.strategy.model.valobj.RuleLogicCheckTypeVO;
import com.itxixi.domain.strategy.repository.IStrategyRepository;
import com.itxixi.domain.strategy.service.annotation.LogicStrategy;
import com.itxixi.domain.strategy.service.rule.ILogicFilter;
import com.itxixi.domain.strategy.service.rule.factory.DefaultLogicFactory;
import com.itxixi.types.common.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Fuzhengwei bugstack.cn @小傅哥
 * @description 【抽奖前规则】根据抽奖权重返回可抽奖范围KEY
 * @create 2024-01-06 09:57
 */
@Slf4j
@Component          //自定义注解 注入之后转map对象进行使用的
                    // 通过此注解，DefaultLogicFactory可以识别该类为权重过滤器，并将其注册到对应的logicFilterMap 中。
@LogicStrategy(logicMode = DefaultLogicFactory.LogicModel.RULE_WIGHT)      //对应抽奖前规则
                    //具体策略
public class RuleWeightLogicFilter implements ILogicFilter<RuleActionEntity.RaffleBeforeEntity> {

    @Resource
    private IStrategyRepository repository;

    public Long userScore = 4500L;

    /**
     * 权重规则过滤；
     * 1. 权重规则格式；4000:102,103,104,105 5000:102,103,104,105,106,107 6000:102,103,104,105,106,107,108,109
     * 2. 解析数据格式；判断哪个范围符合用户的特定抽奖范围
     *
     * @param ruleMatterEntity 规则物料实体对象
     * @return 规则过滤结果
     */
    @Override//RuleActionEntity ：code info ruleModel data(strategyId awardId ruleWeightValueKey）      RuleMatterEntity : userId strategyId awardId ruleModel
    public RuleActionEntity<RuleActionEntity.RaffleBeforeEntity> filter(RuleMatterEntity ruleMatterEntity) {

        log.info("规则过滤-权重范围 userId:{} strategyId:{} ruleModel:{}", ruleMatterEntity.getUserId(), ruleMatterEntity.getStrategyId(), ruleMatterEntity.getRuleModel());

        String userId = ruleMatterEntity.getUserId();
        Long strategyId = ruleMatterEntity.getStrategyId();

        // 查询规则值配置      根据仓储 Long strategyId, Integer awardId, String ruleModel
                            //查询IStrategyRuleDao 根据strategyRuleEntity实体 得到rule_value 4000:102,103,104,105 5000:102,103,104,105,106...
        String ruleValue = repository.queryStrategyRuleValue(ruleMatterEntity.getStrategyId(), ruleMatterEntity.getAwardId(), ruleMatterEntity.getRuleModel());
                                    //StrategyRuleEntity有对应的拆解方法 但这里重新写了

        // 1. 根据用户ID查询用户抽奖消耗的积分值，本章节我们先写死为固定的值。后续需要从数据库中查询。
        Map<Long, String> analyticalValueGroup = getAnalyticalValue(ruleValue);//4000（Long） ： 4000：102,103,104,105（String）

        //11如果Map为空 就放行
        if (null == analyticalValueGroup || analyticalValueGroup.isEmpty()) {
            return RuleActionEntity.<RuleActionEntity.RaffleBeforeEntity>builder()
                    .code(RuleLogicCheckTypeVO.ALLOW.getCode())
                    .info(RuleLogicCheckTypeVO.ALLOW.getInfo())
                    .build();
        }

        //22不为空 判断是否满足条件 然后再接管

        // 2. 转换Keys值，并默认排序  浅拷贝
        List<Long> analyticalSortedKeys = new ArrayList<>(analyticalValueGroup.keySet());///4000 5000 6000
        Collections.sort(analyticalSortedKeys);//4000 5000 6000

        // 3. 找出最小符合的值，也就是【4500 积分，能找到 4000:102,103,104,105】、【5000 积分，能找到 5000:102,103,104,105,106,107】
        Long nextValue = analyticalSortedKeys.stream()
                .filter(key -> userScore >= key)//过滤出小于userScore的key
                .findFirst()//得到第一个小于的 向下取整  有问题 如果5000积分 返回的是四千 应该是五千
                // .max(Long::compare)
                .orElse(null);

        //33如果满足条件
        if (null != nextValue) {
            return RuleActionEntity.<RuleActionEntity.RaffleBeforeEntity>builder()
                    .data(RuleActionEntity.RaffleBeforeEntity.builder()
                            .strategyId(strategyId)//100001
                            .ruleWeightValueKey(analyticalValueGroup.get(nextValue))//4000：102,103,104,105（String）
                            .build())
                    .ruleModel(DefaultLogicFactory.LogicModel.RULE_WIGHT.getCode())
                    .code(RuleLogicCheckTypeVO.TAKE_OVER.getCode())
                    .info(RuleLogicCheckTypeVO.TAKE_OVER.getInfo())
                    .build();
        }

        //44不满足条件
        return RuleActionEntity.<RuleActionEntity.RaffleBeforeEntity>builder()
                .code(RuleLogicCheckTypeVO.ALLOW.getCode())
                .info(RuleLogicCheckTypeVO.ALLOW.getInfo())
                .build();
    }
                            //基本上与getRuleWeightValues()一样 就map的操作不一样  这里动key没动value 相反了
    private Map<Long, String> getAnalyticalValue(String ruleValue) {//4000:102,103,104,105
                                                // strategyRuleEntity有自己的public Map<String, List<Integer>> getRuleWeightValues()
        String[] ruleValueGroups = ruleValue.split(Constants.SPACE);//4000.. 5000.. 6000..
        Map<Long, String> ruleValueMap = new HashMap<>();
        for (String ruleValueKey : ruleValueGroups) {
            // 检查输入是否为空
            if (ruleValueKey == null || ruleValueKey.isEmpty()) {
                return ruleValueMap;
            }
            // 分割字符串以获取键和值
            String[] parts = ruleValueKey.split(Constants.COLON);//4000、102 103 104 105
            if (parts.length != 2) {
                throw new IllegalArgumentException("rule_weight rule_rule invalid input format" + ruleValueKey);
            }
            ruleValueMap.put(Long.parseLong(parts[0]), ruleValueKey);//4000（Long） ： 4000：102,103,104,105（String）
        }
        return ruleValueMap;
    }

}
