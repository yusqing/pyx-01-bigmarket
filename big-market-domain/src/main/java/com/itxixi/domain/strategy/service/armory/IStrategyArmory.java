package com.itxixi.domain.strategy.service.armory;

/**
 * @author Pyx 2024/10/23
 * @description 策略装配工厂
 * @create 2024-10-23 下午5:55
 */
public interface IStrategyArmory {

    /**
     * 装配抽奖策略配置「触发的时机可以为活动审核通过后进行调用
     *
     * @param strategyId 策略ID
     * @return 装配结果
     */
    boolean assembleLotteryStrategy(Long strategyId);


}
